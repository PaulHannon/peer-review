var express = require('express'),
    app = express(),
    bodyParser = require('body-parser'),
    mysql = require('mysql'),
    conn = require('express-myconnection'),
    jwt = require('jsonwebtoken'),
    port = process.env.PORT || 3000,
    routes = require('./routes/index'),
    config = require('./config'),
    count = 0;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/files', express.static('files'));

app.use((req, res, next) => {
    console.log((count++) + ": REQ (" + req.method + ") to " + req.url);

    res.setHeader("Access-Control-Allow-Origin", "http://localhost:9000");
    res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, PATCH, DELETE, HEAD, OPTIONS");
    res.setHeader("Access-Control-Allow-Headers", "Authorization, Content-Type");

    if(req.headers && req.headers.authorization && req.headers.authorization.split(' ')[0] === "Bearer") {
        jwt.verify(req.headers.authorization.split(' ')[1], config.secret, (err, decode) => {
            req.user = err ? undefined : decode;
            next();
        });
    }
    else {
        req.user = undefined;
        next();
    }
});

app.use(
    conn(mysql, {
        host: config.db.host,
        port: config.db.port,
        user: config.db.user,
        password: config.db.password,
        database: config.db.database,
        connectionLimit: config.db.connectionLimit
    }, 'request')
);

app.use('/api', routes);

app.listen(port, () => {
    console.log("Listening on :" + port);
});