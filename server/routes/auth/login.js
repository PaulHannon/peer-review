var express = require('express'),
    bcrypt = require('bcrypt'),
    jwt = require('jsonwebtoken'),
    config = require('../../config'),
    router = express.Router();

router.post('/', (req, res, next) => {
    req.getConnection((err, db) => {
        if(err) return res.sendStatus(500);
        if(!req.body.email || !req.body.password) res.sendStatus(401);

        db.query('SELECT * FROM users WHERE email = ?', [req.body.email], (err, user) => {
            if(err) return next(err);

            if(user.length > 0) {
                if(bcrypt.compareSync(req.body.password, user[0].password)) 
                    return res.json({ token: jwt.sign({ id: user[0].id, forename: user[0].forename, surname: user[0].surname, email: user[0].email }, config.secret)});
                else 
                    return res.sendStatus(401);
            }
            else return res.sendStatus(401);
        });
    });
});

module.exports = router;