var express = require('express'),
    router = express.Router();

router.get('/', (req, res) => {
    req.getConnection((err, db) => {
        if(err) return res.sendStatus(500);

        db.query('SELECT * FROM notifications WHERE user_id = ?', [req.user.id], (err, notifications) => {
            if(err) return res.sendStatus(500);

            res.send(notifications);
        });
    });
});

module.exports = router;