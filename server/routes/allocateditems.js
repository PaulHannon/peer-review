var express = require('express'),
    router = express.Router();

router.get('/', (req, res) => {
    req.getConnection((err, db) => {
        if(err) return res.sendStatus(500);

        var allocatedItemsQuery = 'SELECT workitems.id AS workitem_id, workitems.comments, workitems.filename, workitems.created_date, modules.name AS module_name, assignments.title, assignments.deadline \
                                   FROM workitems \
                                   INNER JOIN allocateditems \
                                   ON workitems.id = allocateditems.workitem_id AND allocateditems.user_id = ? \
                                   INNER JOIN assignments \
                                   ON workitems.assignment_id = assignments.id \
                                   INNER JOIN modules \
                                   ON assignments.module_id = modules.id';

        db.query(allocatedItemsQuery, [req.user.id], (err, rows) => {
            if(err) return res.sendStatus(500);

            return res.send(rows);
        });
    });
});

module.exports = router;