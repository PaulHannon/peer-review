var express = require('express'),
    router = express.Router(),
    modules = require('./modules'),
    workitems = require('./workitems'),
    allocateditems = require('./allocateditems'),
    stats = require('./stats'),
    notifications = require('./notifications'),
    reviews = require('./reviews'),
    login = require('./auth/login');

router.use('/modules', isAuthenticated, modules);
router.use('/workitems', isAuthenticated, workitems);
router.use('/allocateditems', isAuthenticated, allocateditems);
router.use('/stats', isAuthenticated, stats);
router.use('/notifications', isAuthenticated, notifications);
router.use('/reviews', isAuthenticated, reviews);
router.use('/login', login);

function isAuthenticated(req, res, next) {
    return req.user || req.method === "OPTIONS" 
        ? next() 
        : res.sendStatus(401);
}

module.exports = router;