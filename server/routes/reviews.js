var express = require('express'),
    dateFormat = require('date-format'),
    router = express.Router();

router.get('/:id', (req, res) => {
    req.getConnection((err, db) => {
        if(err) res.sendStatus(500);

        db.query('SELECT * FROM reviews WHERE workitem_id = ?', [req.params.id], 
        (err, rows) => {
            if(err) res.sendStatus(500);

            return res.send(rows);
        });
    });
});

router.post('/', (req, res) => {
    req.getConnection((err, db) => {
        if(err) res.sendStatus(500);

        var reviewValues = [
            [req.body.workitemId, req.user.id, req.body.feedbackBody, req.body.marksGiven, dateFormat('yyyy-MM-dd hh:mm', new Date())]
        ];

        db.query('INSERT INTO reviews (`workitem_id`, `user_id`, `feedback`, `marks_given`, `created_date`) VALUES ?', [reviewValues], 
        (err, insertReviewRows) => {
            if(err) return res.sendStatus(500);
            
            db.query('UPDATE usermodules SET num_allocated = num_allocated - 1 WHERE user_id = ? AND module_id = ?', [req.user.id, req.body.moduleId], 
            (err, updateUserModuleRows) => {
                if(err) return res.sendStatus(500);

                db.query('DELETE FROM allocateditems WHERE user_id = ? AND workitem_id = ?', [req.user.id, req.body.workitemId], 
                (err, deleteAllocatedItemRows) => {
                    if(err) return res.sendStatus(500);

                    db.query('SELECT user_id FROM workitems WHERE id = ?', [req.body.workitemId],
                    (err, selectWorkitemRows) => {
                        if(err) return res.sendStatus(500);

                        var notificationValues = [
                            [selectWorkitemRows[0].user_id, "New feedback received!", "Test", "mywork"]
                        ];
                        
                        db.query('INSERT INTO notifications (`user_id`, `title`, `body`, `destination`) VALUES ?', [notificationValues], 
                        (err, insertNotificationRows) => {
                            if(err) return res.sendStatus(500);
                            return res.sendStatus(200);
                        });
                    });
                });
            });
        }); 
    });
});

module.exports = router;