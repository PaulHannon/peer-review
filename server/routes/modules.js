var express = require('express'),
    router = express.Router();

router.get('/', (req, res, next) => {
    req.getConnection((err, db) => {
        if(err) return next(err);

        db.query('SELECT * FROM modules', (err, rows) => {
            if(err) console.log(err);

            res.send(rows);
        });
    });
});

router.get('/assignments', (req, res, next) => {
    req.getConnection((err, db) => {
        if(err) return next(err);

        db.query('SELECT * FROM modules \
                INNER JOIN assignments ON modules.id = assignments.module_id', (err, rows) => {
            if(err) console.log(err);

            res.send(rows);
        });
    });
});

module.exports = router;