var express = require('express'),
    multer = require('multer'),
    crypto = require('crypto'),
    path = require('path'),
    fs = require('fs'),
    dateFormat = require('date-format'),
    router = express.Router(),
    storage = multer.diskStorage({
        destination: (req, file, cb) => cb(null, './files/'),
        filename: (req, file, cb) => {
            var newFilename = req.user.id + '-' + Date.now() + path.extname(file.originalname);
            cb(null, newFilename);
            req.body.originalFilename = newFilename;
        }
    }),
    upload = multer({ storage: storage });

/* CREATE NEW WORKITEM & ALLOCATE REVIEWERS */
router.post('/', upload.single('workitem'), (req, res) => {
    req.getConnection((err, db) => {
        if(err) return res.sendStatus(500);

        var workItemValues = [
            parseInt(req.body.assignment_id), 
            parseInt(req.body.user_id), 
            req.body.comments, 
            req.body.originalFilename, 
            dateFormat('yyyy-MM-dd hh:mm', new Date())
        ];

        db.query('INSERT INTO workitems (`assignment_id`, `user_id`, `comments`, `filename`, `created_date`) VALUES (?, ?, ?, ?, ?)', workItemValues, (err, workitem) => {
                if(err) { console.log(err); return res.sendStatus(500); }

                var userModuleSelectValues = [
                    req.body.user_id, 
                    req.body.module_id
                ];

                db.query('SELECT * FROM usermodules WHERE `user_id` <> ? AND `module_id` = ? AND `num_allocated` < 2', userModuleSelectValues, (err, usermodules) => {
                    if(err) { console.log(err); return res.sendStatus(500); }

                    var allocatedItemValues = [[workitem.insertId, usermodules[0].user_id]];
                    if(usermodules.length > 1) allocatedItemValues.push([workitem.insertId, usermodules[1].user_id]);

                    db.query('INSERT INTO allocateditems (`workitem_id`, `user_id`) VALUES ?', [allocatedItemValues], (err, allocated) => {
                        if(err) { console.log(err); return res.sendStatus(500); }

                        var userModuleUpdateValues = [
                            req.body.module_id,
                            usermodules[0].user_id,
                            usermodules[1] ? usermodules[1].user_id : null
                        ];
                        
                        db.query('UPDATE usermodules SET num_allocated = num_allocated + 1 WHERE module_id = ? AND (user_id = ? OR user_id = ?)', userModuleUpdateValues, (err) => {
                            if(err) { console.log(err); return res.sendStatus(500); }

                            var notificationValues = [[usermodules[0].user_id, 'You have feedback to complete!', 'Test', 'assignedwork']];
                            if(usermodules.length > 1) notificationValues.push([usermodules[1].user_id, 'You have feedback to complete!', 'Test', 'assignedwork']);
                            
                            db.query('INSERT INTO notifications (`user_id`, `title`, `body`, `destination`) VALUES ?', [notificationValues], (err, rows) => {
                                if(err) { console.log(err); return res.sendStatus(500); }
                                else return res.sendStatus(200);
                            });
                        });
                    });
                });
        });
    });
});

router.get('/', (req, res) => {
    req.getConnection((err, db) => {
        if(err) return res.sendStatus(500);

        db.query('SELECT workitems.id, assignments.title, workitems.created_date, modules.name AS `module_name` \
                FROM workitems \
                INNER JOIN assignments ON workitems.assignment_id = assignments.id \
                INNER JOIN modules ON assignments.module_id = modules.id \
                WHERE workitems.user_id = ?', [req.user.id],
        (err, rows) => {
            if(err) return res.sendStatus(500);
            return res.send(rows);
        });
    });
});

router.get('/:id', (req, res) => {
    req.getConnection((err, db) => {
        if(err) return res.sendStatus(500);

        db.query('SELECT * FROM workitems \
                INNER JOIN assignments ON workitems.assignment_id = assignments.id \
                INNER JOIN modules ON assignments.module_id = modules.id \
                WHERE workitems.id = ? AND workitems.user_id = ?', [
            req.params.id, 
            req.user.id
        ],
        (err, rows) => {
            if(err) return res.sendStatus(500);
            return res.send(rows);
        });
    });
});

router.get('/:id/document', (req, res) => {
    req.getConnection((err, db) => {
        if(err) return res.sendStatus(500);

        db.query('SELECT * FROM workitems WHERE id = ?', [req.params.id], (err, rows) => {
            var workitem = rows[0];
            var allowedExtensions = ['.APPCACHE', '.APS', '.ARSC', '.AS', '.ASM', '.ASP', '.ASPX', '.B', '.BAS', '.C', '.CC', '.CLASS', '.COB', '.CONFIG', '.CPP', '.CRT', '.CS', '.CSHTML', '.CSP', '.CSPROJ', '.CSR', '.CSS', '.CSX', '.CXX', '.D', '.DHTML', '.ERB', '.ERL', '.FS', '.FSI', '.FSPROJ', '.GITATTRIBUTES', '.GITIGNORE', '.H', '.HH', '.HPP', '.HTM', '.HTML', '.HXX', '.JAVA', '.JHTML', '.JS', '.JSON', '.JSP', '.JSPX', '.LESS', '.LOG', '.MAK', '.MARKDOWN', '.MAP', '.MHTML', '.MK', '.MM', '.MO', '.MYAPP', '.NUPKG', '.NUSPEC', '.O', '.PHP', '.PY', '.PYD', '.PYW', '.R', '.README', '.RBW', '.RHTML', '.RSS', '.RTF', '.SASS', '.SRC', '.TXT', '.TS', '.VBHTML', '.VCPROJ', '.VCXPROJ', '.XAML', '.XCODEPROJ', '.XHT', '.XML', '.XQ', '.XSD', '.XSS', '.YAML'];

            if(allowedExtensions.indexOf(path.extname(workitem.filename.toUpperCase())) > -1)
                return res.send(fs.readFileSync("files/" + workitem.filename).toString().split("\n"));
            else 
                return res.send([]);
        });
    });
});

router.get('/:id/review', (req, res) => {
    req.getConnection((err, db) => {
        if(err) return res.sendStatus(500);

        db.query('SELECT * FROM workitems \
                INNER JOIN assignments ON workitems.assignment_id = assignments.id \
                WHERE workitems.id = ?',  [req.params.id],
        (err, rows) => {
            if(err) return res.sendStatus(500);
            return res.send(rows);
        });
    });
});

function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
}

module.exports = router;