var express = require('express'),
    router = express.Router(),
    totalReviews = 0,
    totalWorkItems = 0;

router.get('/lastmonth', (req, res, next) => {
    req.getConnection((err, db) => {
        if(err) return next(err);

        db.query('SELECT * FROM reviews WHERE user_id = ? AND created_date BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND NOW();', [req.user.id], (err, reviews) => {
            if(err) console.log(err);

            totalReviews = reviews.length;

            db.query('SELECT * FROM workitems WHERE user_id = ? AND created_date BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND NOW();', [req.user.id], (err, workitems) => {
                if(err) console.log(err);

                res.send({ "totalReviews": totalReviews, "totalWorkItems": workitems.length });
            });
        });
    });
});

module.exports = router;