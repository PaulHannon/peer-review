import { autoinject } from "aurelia-framework";
import { Router } from "aurelia-router";
import { AuthService } from "aurelia-authentication";
import { ValidationControllerFactory, ValidationController, ValidationRules, validateTrigger } from "aurelia-validation";
import { FormRenderer } from "../../form-renderer";
import { WebService } from "../../services/web-service";
import { Module } from "../../model/module";
import { Assignment } from "../../model/assignment";
import { Progress } from "../../resources/components/progress";
import * as $ from "jquery";

@autoinject()
export class Submit {
    title: string;
    comments: string;

    module: Module;
    modules: Array<Module>;

    assignment: Assignment;
    assignments: Array<Assignment>;
    selectedAssignments: Array<Assignment>;

    workitem: FileList;
    formData: FormData;
    submitModal: any;

    controller: ValidationController;

    constructor(
        private webService: WebService, 
        private authService: AuthService,
        private router: Router,
        private validationFactory: ValidationControllerFactory,
        private progress: Progress
    ) {
        this.formData = new FormData();
        this.modules = new Array<Module>();
        this.assignments = new Array<Assignment>();
        
        this.webService.get("modules").then(res => res.forEach(module => this.modules.push(module)));
        this.webService.get("modules/assignments").then(res => {
            res.forEach(assignment => this.assignments.push(assignment));
            this.selectedAssignments = this.assignments.filter(assignment => assignment.module_id === this.assignments[0].module_id);
        });

        this.configureController();
        this.applyValidationRules();
    }

    moduleChanged(value: any) {
        this.selectedAssignments = this.assignments.filter(assignment => assignment.module_id === value.id);
        this.assignment = this.selectedAssignments[0];
    }

    submit() {
        this.submitModal = $("#submit-modal");

        this.controller.validate().then(result => {
            if(!result.valid)
                return;

            this.submitModal.modal({ backdrop: "static" });
            this.submitModal.modal("show");

            this.formData.append("assignment_id", this.assignment.id.toString());
            this.formData.append("user_id", this.authService.getTokenPayload()["id"]);
            this.formData.append("module_id", this.module.id.toString());
            this.formData.append("comments", this.comments);
            this.formData.append("workitem", this.workitem ? this.workitem[0] : null);
    
            this.webService.post("workitems", this.formData);
    
            this.submitModal.on("hide.bs.modal", event => this.router.navigateToRoute("mywork"));

            this.progress.simulateLoad("#submit-modal");
        });
    }

    configureController() {
        this.controller = this.validationFactory.createForCurrentScope();
        this.controller.addRenderer(new FormRenderer());
        this.controller.validateTrigger = validateTrigger.manual;
    }

    applyValidationRules() {
        ValidationRules
            .ensure((s: Submit) => s.module).required()
            .ensure((s: Submit) => s.assignment).required()
            .ensure((s: Submit) => s.workitem).required().withMessage("You must upload a file")
            .on(this);
    }
}