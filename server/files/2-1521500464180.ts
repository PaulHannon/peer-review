export default {
    endpoint: "api",
    logoutRedirect: "#/login",
    loginUrl: "login",  
    signupUrl: "register",
    profileUrl: "account",
    accessTokenName: "token",
    loginOnSignup: true,
    storageChangedReload: 1
};