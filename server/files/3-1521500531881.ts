import { autoinject } from "aurelia-framework";
import { RouterConfiguration, Router } from "aurelia-router";
import { WebService } from "../services/web-service";

@autoinject()
export class MyWork {
    recentlySubmitted: Array<JoinedWorkItemAssignment>;

    constructor(private router: Router, private webService: WebService) {  }

    activate() {
        this.webService.get("workitems")
        .then(res => {
            this.recentlySubmitted = res.sort((a, b) => { return new Date(b.created_date).getTime() - new Date(a.created_date).getTime(); });
            this.checkNew();
        });
    }

    submitWork() {
        this.router.navigateToRoute("submit");
    }

    checkNew() {
        // This notifies the user with a NEW badge if the workitem is less than 10 minutes old; 600000 ms = 10 minutes
        this.recentlySubmitted.filter(workitem => workitem.isNew = (Date.parse(workitem.created_date) > Date.now() - 600000) );
    }
}

interface JoinedWorkItemAssignment {
    id: number;
    title: string;
    module_name: string;
    created_date: string;
    isNew: boolean;
}