CREATE TABLE reviews(
    id int NOT NULL AUTO_INCREMENT,
    workitem_id int NOT NULL,
    user_id int NOT NULL,
    feedback TEXT NOT NULL,
    marks_given int,
    PRIMARY KEY (id),
    FOREIGN KEY (workitem_id) REFERENCES workitems(id),
    FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE workitems(
    id int NOT NULL AUTO_INCREMENT,
    module_id int NOT NULL,
    user_id int NOT NULL,
    title TEXT NOT NULL,
    deadline DATETIME,
    marks int,
    PRIMARY KEY (id),
    FOREIGN KEY (module_id) REFERENCES modules(id),
    FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE users(
    id int NOT NULL AUTO_INCREMENT,
    forename VARCHAR(255) NOT NULL,
    surname VARCHAR(255) NOT NULL,
    email TEXT NOT NULL,
    password TEXT NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE criteria(
    id int NOT NULL AUTO_INCREMENT,
    workitem_id int NOT NULL,
    description TEXT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (workitem_id) REFERENCES workitems(id)
);

CREATE TABLE modules(
    id int NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
);