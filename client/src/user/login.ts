import { autoinject } from "aurelia-framework";
import { AuthService } from "aurelia-authentication";
import { Config, Rest } from "aurelia-api";
import { ValidationControllerFactory, ValidationController, ValidationRules, validateTrigger } from "aurelia-validation";
import { FormRenderer } from "../form-renderer";

@autoinject()
export class Login {
    api: Rest;

    email: string;
    password: string;
    error: boolean;

    controller: ValidationController;

    constructor(
        private authService: AuthService, 
        private config: Config,
        private validationFactory: ValidationControllerFactory
    ) {
        this.api = config.getEndpoint("api");

        this.configureController();
        this.applyValidationRules();
    }

    login() {
        this.error = false;

        this.controller.validate().then(result => {
            if(!result.valid)
                return;

            this.authService.login(this.email, this.password)
            .catch(err => this.error = (err.status === 401));
        });
    }

    configureController() {
        this.controller = this.validationFactory.createForCurrentScope();
        this.controller.addRenderer(new FormRenderer());
        this.controller.validateTrigger = validateTrigger.manual;
    }

    applyValidationRules() {
        ValidationRules
            .ensure((l: Login) => l.email).required()
            .ensure((l: Login) => l.password).required()
            .on(this);
    }
}