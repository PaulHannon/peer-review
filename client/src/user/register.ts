import { autoinject } from "aurelia-framework";
import { AuthService } from "aurelia-authentication";

@autoinject()
export class Register {
    email: string;
    password: string;
    repassword: string;

    constructor(private authService: AuthService) {  }

    register() {
        return this.authService.signup(this.email, this.password)
        .catch(error => console.log(error));
    }
}