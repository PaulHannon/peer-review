import { autoinject } from "aurelia-framework";
import { Router } from "aurelia-router";
import { WebService } from "../services/web-service";
import { Notification } from "../model/notification";

@autoinject()
export class Dashboard {
    private stats: Stats;
    private notifications: Array<Notification>;

    constructor(private webService: WebService, private router: Router) {  }

    activate() {
        this.webService.get("stats/lastmonth")
        .then(res => this.stats = res);

        this.webService.get("notifications")
        .then(res => { this.notifications = res.filter(notification => notification.unread === "true" ); this.notifications.reverse(); });
    }

    goToRoute(route: string) {
        return this.router.navigateToRoute(route);
    }
}

interface Stats {
    totalReviews: number;
    totalWorkItems: number;
}