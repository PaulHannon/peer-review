import { autoinject } from "aurelia-framework";
import { Router } from "aurelia-router";
import { WebService } from "../../services/web-service";
import { Progress } from "../../resources/components/progress";
import { Line } from "../../model/line";
import { LineComment } from "../../model/linecomment";
import * as $ from "jquery";

@autoinject()
export class Review {
    id: string;
    lines: Array<Line> = new Array<Line>();
    review: any;

    currentLineNumber: number;
    currentComment: string;
    lineComments: Array<LineComment> = new Array<LineComment>();

    additionalComments: string;
    marksGiven: string;

    reviewModal: any;
    submitReviewModal: any;

    constructor(
        private webService: WebService, 
        private progress: Progress,
        private router: Router
    ) {  }

    activate(params: any) {
        this.id = params.id;
    }

    attached() {
        this.reviewModal = $("#review-modal");
        this.submitReviewModal = $("#submit-review-modal");

        this.webService.get("workitems/" + this.id + "/review")
        .then(res => this.review = res[0]);

        this.webService.get("workitems/" + this.id + "/document")
        .then(res => {
            if(res.length > 0)
                res.forEach(line => this.lines.push(line.length > 0 ? { contents: line, hasComment: false } : { contents: " ", hasComment: false }));
        });
    }

    commentsToString(): string {
        return JSON.stringify(this.lineComments) + " ~ " + this.additionalComments;
    }

    submitReview() {
        this.submitReviewModal.modal({ backdrop: "static" });
        this.submitReviewModal.modal("show");

        this.progress.simulateLoad("#submit-review-modal");

        this.webService.post("reviews", { 
            "workitemId": this.id, 
            "feedbackBody": this.commentsToString(), 
            "marksGiven": parseInt(this.marksGiven),
            "moduleId": this.review.module_id
        }, true);

        this.submitReviewModal.on("hide.bs.modal", event => this.router.navigateToRoute("assignedwork"));
    }
    
    showCommentModal(lineNumber: number) {
        this.reviewModal.modal("show");
        this.currentLineNumber = lineNumber;

        if(this.lineComments.find(c => { return c.lineNumber === lineNumber; }))
            this.currentComment = this.lineComments.find(c => { return c.lineNumber === lineNumber; }).comment;
        else
            this.currentComment = undefined;
    }

    addComment() {
        let lineIndex;
        let lineComment = { lineNumber: this.currentLineNumber, comment: this.currentComment };
        let lineExists = this.lineComments.find((c, i) => {
            if(c.lineNumber === this.currentLineNumber) {
                lineIndex = i;
                return true;
            }
        });

        if(!lineExists) 
            this.lineComments.push(lineComment);
        else
            this.lineComments[lineIndex] = lineComment;

        this.lines[this.currentLineNumber].hasComment = true;

        this.currentLineNumber = undefined;
        this.currentComment = undefined;

        this.reviewModal.modal("hide");
    }
}