import { autoinject } from "aurelia-framework";
import { WebService } from "../services/web-service";
import { Notification } from "../model/notification";

@autoinject()
export class Notifications {
    unread: Array<Notification> = new Array<Notification>();
    read: Array<Notification> = new Array<Notification>();

    constructor(private webService: WebService) {  }

    activate() {
        this.webService.get("notifications")
        .then(res => {
            this.unread = res.filter(notification => notification.unread === "true");
            this.read = res.filter(notification => notification.unread === "false");
        });
    }
}