import { autoinject } from "aurelia-framework";
import { WebService } from "../services/web-service";

@autoinject()
export class AssignedWork {
    allocatedItems: Array<any> = new Array<any>();

    constructor(private webService: WebService) {  }

    activate() {
        this.webService.get("allocateditems")
        .then(res => this.allocatedItems = res);
    }
}