export interface Notification {
    id: number;
    user_id: number;
    title: string;
    body: string;
    destination: string;
}