export interface Assignment {
    id: number;
    module_id: number;
    title: string;
    description: string;
    deadline: Date;
    total_marks: number;
}