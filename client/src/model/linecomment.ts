export interface LineComment {
    lineNumber: number;
    comment: string;
}