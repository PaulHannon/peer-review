export interface Line {
    contents: string;
    hasComment: boolean;
}