import { Aurelia } from "aurelia-framework";
import environment from "./environment";
import authConfig from "./auth-config";

export function configure(aurelia: Aurelia) {
    aurelia.use
        .standardConfiguration()
        .feature("resources")
        .plugin("aurelia-api", config => {
            config.registerEndpoint("api", "http://localhost:3000/api/", { headers: {
                "Accept": "application/json, multipart/form-data"
            }});
        })
        .plugin("aurelia-authentication", baseConfig => {
            baseConfig.configure(authConfig);
        })
        .plugin("aurelia-validation")
        .globalResources("./resources/value-converters/display-date");

    if (environment.debug) {
        aurelia.use.developmentLogging();
    }

    if (environment.testing) {
        aurelia.use.plugin("aurelia-testing");
    }

    aurelia.start().then(() => aurelia.setRoot());
}
