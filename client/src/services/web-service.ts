import { autoinject } from "aurelia-framework";
import { EventAggregator } from "aurelia-event-aggregator";
import { Config, Rest } from "aurelia-api";
import { AuthService } from "aurelia-authentication";

@autoinject()
export class WebService {
    api: Rest;
    response: any;

    constructor(
        private apiConfig: Config, 
        private authService: AuthService,
        private eventAggregator: EventAggregator
    ) {
        this.api = apiConfig.getEndpoint("api");
        this.api.client.configure(config => {
            config.withInterceptor({
                request(request) {
                    let jwt = JSON.parse(localStorage.getItem("aurelia_authentication"));

                    if(jwt)
                        request.headers.set("Authorization", "Bearer " + jwt.token);

                    return request;
                }
            });
        });
    }

    get(endpoint: string): Promise<any> {
        return this.api.find(endpoint);
    }

    post(endpoint: string, data: any, isJson?: boolean) {
        if(isJson)
            return this.api.post(endpoint, data, { headers: { "Content-Type": "application/json" } });
        else
            return this.api.post(endpoint, data);
    }

    update(endpoint: string) {

    }
}