import { autoinject } from "aurelia-framework";
import { WebService } from "../services/web-service";
import { Line } from "../model/line";
import { LineComment } from "../model/linecomment";

@autoinject()
export class WorkItemRecord {
    workitem: any;
    reviews: Array<any>;
    lines: Array<Partial<Line>> = new Array<Partial<Line>>();
    comments: Array<LineComment> = new Array<LineComment>();
    modalComments: Array<LineComment> = new Array<LineComment>();

    commentModal: any;

    constructor(private webService: WebService) {  }

    activate(param: any) {
        this.webService.get("workitems/" + param.id)
        .then(res => this.workitem = res[0]);

        this.webService.get("workitems/" + param.id + "/document")
        .then(res => {
            if(res.length > 0)
                res.forEach((line, index) => this.lines.push(line.length > 0 ? { contents: line, hasComment: false } : { contents: " ", hasComment: false }));

            this.webService.get("reviews/" + param.id)
            .then(reviews => {
                reviews.forEach(review => {
                    let tempFeedback = review.feedback;
                    review.feedback = JSON.parse(tempFeedback.split(" ~ ")[0]);
                    review.additionalComments = tempFeedback.split(" ~ ")[1];
                    review.feedback.forEach(feedback => {
                        this.lines[feedback.lineNumber].hasComment = true;
                        this.comments.push(feedback);
                    });
                });
                this.reviews = reviews;
            });
        });
    }

    attached() {
        this.commentModal = $("#workitem-record-modal");
    }

    showCommentModal(lineNumber: number) {
        this.modalComments = this.comments.filter(val => val.lineNumber === lineNumber);
        this.commentModal.modal("show");
    }
}