import { ValidationRenderer, RenderInstruction, ValidateResult } from "aurelia-validation";

export class FormRenderer {
    // This function adds validation errors to the form so that the user can see what they've gotten wrong
    render(instruction: RenderInstruction) {
        for(let { result, elements } of instruction.unrender) {
            for(let element of elements) {
                this.remove(element, result);
            }
        }

        for(let { result, elements } of instruction.render) {
            for(let element of elements) {
                this.add(element, result);
            }
        }
    }

    add(element: Element, result: ValidateResult) {
        if(result.valid) {
            return;
        }

        const formGroup = element.closest(".form-group");
        if(!formGroup) {
            return;
        }

        element.classList.add("is-invalid");
        formGroup.getElementsByTagName("label")[0].classList.add("text-danger");

        // Add the appropriate styling classes
        const message = document.createElement("span");
        message.className = "form-text validation-message text-danger";
        message.textContent = result.message;
        message.id = `validation-message-${result.id}`;
        formGroup.appendChild(message);
    }

    remove(element: Element, result: ValidateResult) {
        if(result.valid) {
            return;
        }

        const formGroup = element.closest(".form-group");
        if(!formGroup) {
            return;
        }

        // Remove the appropriate styling classes
        const message = formGroup.querySelector(`#validation-message-${result.id}`);
        if(message) {
            formGroup.removeChild(message);
            formGroup.getElementsByTagName("label")[0].classList.remove("text-danger");

            if(formGroup.querySelectorAll(".form-text.validation-message.text-danger").length === 0) {
                element.classList.remove("is-invalid");
            }
        }
    }
}