import { autoinject, computedFrom } from "aurelia-framework";
import { RouterConfiguration, Router } from "aurelia-router";
import { AuthService, AuthenticateStep } from "aurelia-authentication";
import * as $ from "jquery";
import "bootstrap";

@autoinject()
export class App {
    name: string;

    constructor(private router: Router, private authService: AuthService) {  }

    configureRouter(config: RouterConfiguration, router: Router) {
        config.title = "Peer Review";
        config.addPipelineStep("authorize", AuthenticateStep);
        config.map([
            { route: ["", "dashboard"], name: "dashboard", moduleId: "core/dashboard", nav: true, title: "Dashboard", auth: true, settings: { icon: "home" } },

            { route: "mywork", name: "mywork", moduleId: "core/mywork", nav: true, title: "My Work", auth: true, settings: { icon: "edit" } },
            { route: "mywork/submit", name: "submit", moduleId: "core/mywork-sub/submit", title: "Submit", auth: true },

            { route: "assignedwork", name: "assignedwork", moduleId: "core/assignedwork", nav: true, title: "Assigned Work", auth: true, settings: { icon: "hourglass-half" } },
            { route: "assignedwork/review/:id", name: "review", moduleId: "core/assignedwork-sub/review", title: "Review", auth: true },

            { route: "awards", name: "awards", moduleId: "core/awards", nav: true, title: "Awards", auth: true, settings: { icon: "trophy" } },
            { route: "notifications", name: "notifications", moduleId: "core/notifications", nav: true, title: "Notifications", auth: true, settings: { icon: "bell" } },

            { route: "login", name: "login", moduleId: "user/login", title: "Login" },
            { route: "register", name: "register", moduleId: "user/register", title: "Register" },

            { route: "workitem/:id", name: "workitem", moduleId: "records/workitem-record", title: "Work Item", auth: true }
        ]);

        this.router = router;
    }

    get isMobile(): boolean {
        return $("body").width() < 768;
    }

    get fullName(): string {
        return this.authService.getTokenPayload()["forename"] + " " + this.authService.getTokenPayload()["surname"];
    }

    @computedFrom("authService.authenticated")
    get authenticated() {
        return this.authService.authenticated;
    }

    @computedFrom("router.currentInstruction.config.name")
    get route() {
        return this.router.currentInstruction && this.router.currentInstruction.config.name;
    }

    logout() {
        this.authService.logout("#/login");
    }
}

