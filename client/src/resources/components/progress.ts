import * as $ from "jquery";

export class Progress {
    modal: any;

    simulateLoad(modalId: string) {
        this.modal = $(modalId);
        let progress = 0;

        let wait = setInterval(() => {
            if(progress > 200) {
                this.modal.modal("hide"); 
                clearInterval(wait);
            }

            $(".progress-bar").css("width", progress + "%").attr("aria-valuenow", progress);
            progress++;
        }, 20);
    }
}