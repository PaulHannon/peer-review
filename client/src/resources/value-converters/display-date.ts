import * as moment from "moment";

export class DisplayDateValueConverter {
    toView(value) {
        return value === "undefined" || value === undefined ? null : moment(value).format("Do MMM YYYY @ HH:mm");
    }
}